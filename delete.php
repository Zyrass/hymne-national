<?php

session_start();

if (!$_SESSION['admin'] || !isset($_POST['id'])) {
  header("Location: index.php");
}

  // Connexion à la base de donnée
  require_once './bdd/connexion.php';

  // Requête 1 : Pour sélectionner l'id et le pays
  $select_sql_id_country = "SELECT id, country FROM nation WHERE id >= 1 ORDER BY country ASC";
  $req = $bdd->prepare( $select_sql_id_country );

  // Exécution de la requête
  $executeIsOk = $req->execute();

  // Vérification de l'exécution de la requête
  if (!$executeIsOk) {
    echo "<div class='debug'>";
      echo $message = "L'exécution n'a pas fonctionné.";
    echo "</div>";
    $req->closeCursor();
  }

  // récupération de toutes les données enregistré dans la base de donnée
  $resultat = $req->fetchAll(PDO::FETCH_ASSOC);

  // Vérification si le nombre de ligne est supérieur à 0
  $nombreDeLignes = count($resultat);

  if ($nombreDeLignes <= 1 ) {
    // echo "<div class='debug'>";
    //   echo $message = "Il y a actuellement 0 pays d'enregistré";
    // echo "</div>";
    $req->closeCursor();
  }

  // Fermeture de la connexion
  $req->closeCursor();

  $page = "Supprimer";
  include_once './includes/header.php';
  include_once './includes/navigation.php';
?>


<div class="container">
  <div class="row">

  </div><!-- /.row -->

    <h1 class="white-text center-align">Supprimer</h1>
    <hr class="my-5" />
    <blockquote class="blue-grey-text flow-text">
      Vous vous trouvez sur la page de <strong>suppression</strong>.<br /><br />
      <strong>Attention : </strong>Toute suppression est <strong>irréversible</strong>. Réfléchissez bien avant de tenter une quelconque <strong>suppression</strong>.
    </blockquote>

    <h2 class="white brown-text text-darken-4 center-align py-1 my-4">Liste</h2>

    <table class="striped highlight centered white p-2 mb-8">
      <thead class="center-align">
        <tr>
          <th>#</th>
          <th>Nom du pays</th>
          <th>Supprimer</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach( $resultat as $key => $value ) : ?>

        <tr>
          <td><?= $value['id'] ?></td>
          <td><?= $value['country'] ?></td>
          <td>
            <!-- Modal Trigger -->
            <a href="#modal-<?= $value['id'] ?>" class="waves-effect waves-light btn modal-trigger red darken-4"><i class="material-icons">close</i></a>
          </td>
        </tr>

        <?php endforeach ?>
      </tbody>
    </table>

    <?php foreach( $resultat as $key => $value ) : ?>

    <!-- Modal Structure -->
    <div id="modal-<?= $value['id'] ?>" class="modal">
      <div class="modal-content">
        <h4>Confirmation</h4>
        <hr />
        <p>Êtes-vous bien sûr de vouloir supprimer le pays "<span><strong class="red-text text-lighten-1"><?= $value['country'] ?></strong></span>" de la base de donnée ?</p>
        <p class="red-text text-lighten-1">Ceci est irréversible</p>
      </div>
      <div class="modal-footer">
        <a
          href="treatments/treatments_delete.php?identifiant=<?= $value['id'] ?>&pays=<?= $value['country'] ?>"
          class="modal-close waves-effect waves-green red lighten-1 btn btn-small"
        >Oui je suis sûr</a>
      </div>
    </div>

    <?php endforeach ?>




</div><!-- /.container -->

<?php include_once './includes/footer.php'; ?>