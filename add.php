<?php
session_start();

if (!$_SESSION['modo'] || !$_SESSION['admin']) {
  $_SESSION['errors'] = $error = "Vous n'avez pas accès à cette page au vue des privilèges qui vous sont accordé.";
  header("Location: index.php");
}

$page = "Ajouter";
include_once './includes/header.php';
include_once './includes/navigation.php';
?>


<div class="container">
  <div class="row">

  </div><!-- /.row -->

  <h1 class="white-text center-align">Ajouter</h1>
  <hr class="my-5" />
  <blockquote class="blue-grey-text flow-text">
    <strong>Ajoutez un nouvel hymne national</strong> dans la base de donnée.. <br />
    Pour que ce soit efficace j'utilise <strong>wikipédia</strong> et je fais également mes recherches sur <strong>google</strong> et <strong>youtube</strong>.
  </blockquote>

  <h2 class="white brown-text text-darken-4 center-align py-1 my-4">Formulaire</h2>

  <form action="treatments/treatments_add.php" method="post" class="col s12 mb-8">

    <!-- Drapeau -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">flag</i>
      <input placeholder="URL du drapeau" id="flag" type="url" class="validate" name="flag" />
      <label for="flag">Drapeau</label>
    </div>

    <!-- Pays -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">language</i>
      <input placeholder="Nom du pays" id="country" type="text" class="validate" name="country" />
      <label for="country">Pays</label>
    </div>

    <!-- Titre de l'Hymne -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">title</i>
      <input placeholder="Titre d'origine" id="title" type="text" class="validate" name="title" />
      <label for="title">Titre</label>
    </div>

    <!-- Lien wikipédia du pays concerné -->
    <div class="row">
      <div class="input-field col s12 mt-1">
        <i class="material-icons prefix amber-text">link</i>
        <input placeholder="Lien wikipédia du pays concerné" id="wikipedia" type="url" class="validate" name="wikipedia" />
        <label for="wikipedia">wikipédia</label>
      </div>

      <div class="p-1">
        <strong>Exemple du lien wikipédia : </strong> <small><em>https://fr.wikipedia.org/wiki/France</em></small>
      </div>
    </div>

    <!-- Lien vers la vidéo youtube -->
    <div class="row">
      <div class="input-field col s12 mt-1">
        <i class="material-icons prefix amber-text">link</i>
        <input placeholder="Lien vidéo youtube." id="youtube" type="url" class="validate" name="youtube" />
        <label for="youtube">youtube</label>
      </div>
      <div class="p-1">
        <strong>Exemple du lien vidéo : </strong> <small><em>https://www.youtube.com/embed/JtdAnUR4hVM</em></small>
      </div>
    </div>

    <!-- Description -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="description" class="materialize-textarea" name="description"></textarea>
      <label for="description">Description</label>
    </div>

    <!-- Paroles original -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="lyrics" class="materialize-textarea" name="lyrics"></textarea>
      <label for="lyrics">Paroles original</label>
    </div>

    <!-- Paroles Traduite -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="translate" class="materialize-textarea" name="translate"></textarea>
      <label for="translate">Paroles tratuite en français</label>
    </div>

    <div class="row center-align mt-5">
      <div class="col s7">
        <button type="submit" class="btn waves-effect waves-light"><i class="material-icons left hide-on-small-only">save</i>Enregistré</button>
      </div>
      <div class="col s5">
        <button class="btn waves-effect waves-light red lighten-1" type="reset"><i class="material-icons left hide-on-small-only">clear</i>Effacé</button>
      </div>
    </div>


  </form>
</div><!-- /.container -->

<?php include_once './includes/footer.php'; ?>