<?php

session_start();

$pseudo = htmlspecialchars(trim($_POST['pseudo']));
$password = htmlspecialchars(trim($_POST['mdp']));

// echo var_dump($pseudo);
// echo "<br />";
// echo var_dump($password);

if (isset($password) && !empty($password) && isset($pseudo) && !empty($pseudo)) {

  // Connexion à la base de donnée
  include_once "./bdd/connexion.php";

  // Selection de tous les utilisateurs de la table "user"
  $select = "SELECT grade, pseudo, mdp FROM user WHERE pseudo LIKE :pseudo AND mdp LIKE :mdp LIMIT 0,1";
  $req = $bdd->prepare($select);

  // Liaison des marqueurs
  $req->bindValue(":pseudo", $pseudo, PDO::PARAM_STR);
  $req->bindValue(":mdp", $password, PDO::PARAM_STR);

  // Exécution de la requête
  $executeIsOk = $req->execute();

  if (!$executeIsOk) {
    $error = "Désolé il y a eu une erreur lors de l'exécution de la requête";
  }

  // Récupération de la data
  $resultat = $req->fetch(PDO::FETCH_OBJ);

  // Combien de résultat ont été trouvé
  $combienDeLigne = count($resultat);

  if ($combienDeLigne > 0) {

    if ($pseudo === $resultat->pseudo && $password === $resultat->mdp) {

      if ($resultat->grade === "1") {
        $role = "Modérateur";
        $_SESSION['modo'] = $role;
        $message = "Vous êtes connecté en tant que modérateur";
        header("Location: index.php");
      } else if ($resultat->grade === "2") {
        $role = "Administrateur";
        $_SESSION['admin'] = $role;
        $message = "Vous êtes connecté en tant qu'administrateur";
        header("Location: index.php");
      } else {
        $error = "Bon c'est quoi le problème";
      }
    } else {
      $error = "Identifiant invalide";
    }
  } else {
    $error = "Désolé aucun résultat n'a été trouvé";
  }
} else {
  $error = "Désolé, mais il y a une erreur avec les identifiants";
  // header("Location: index.php");
}

?>

<div class="container">
  <div class="row">
    <h1>
      <?php
      if (isset($message)) {
        echo $message;
      } else if (isset($error)) {
        echo $error;
      } else {
        echo $combienDeLigne;
      }
      ?>
    </h1>
  </div>
</div>