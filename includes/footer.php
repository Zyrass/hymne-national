<?php session_start(); ?>
<footer class="page-footer mt-8">
    <div class="container">
      <div class="row">
        <div class="col s12 offset-m1 m10 center-align" id="btn_container">

          <?php if (
            $page === "Ajouter" ||
            $page === "Modifier" ||
            $page === "Supprimer" ||
            $page === "Nation" ||
            $page === "Traitement suppression" ||
            $page === "Liste" ||
            $page === "Search" ||
            $page === "Panel-Admin" ||
            $page === "Panel-Modération" ||
            $page === "Panel-Visiteur"
          ) : ?>

            <a class="waves-effect waves-light btn btn-block btn-small blue lighten-1 mt-1" href="index.php">
              <i class="material-icons left">home</i>Retour à l'accueil
            </a>

          <?php else : ?>

            <p class="hide-on-large-only blue-grey-text text-lighten-4">Pour ajouter un élément uniquement sur <strong>mobile</strong> et <strong>tablette</strong>, il faut allez voir le contenu d'un quelconque hymne national, se rendre dans le "<strong>menu</strong>", et cliquer sur "<strong>connexion</strong>"
              <br /><br />
              Vous aurez ainsi une modale qui s'ouvrira afin de saisir des identifiants qui vous ont été transmis.
            </p>

          <?php endif ?>

          <?php if (isset($_SESSION['admin']) || isset($_SESSION['modo'])) : ?>
            <button type="button" id="logout" class="waves-effect waves-light btn btn-block btn-small red white-text mt-1"><i class="material-icons left">lock</i> Se déconnecter</button>
          <?php endif ?>

          <!-- Modal Structure pour connexion au panel Admin -->
          <div id="modal3WA" class="modal brown white-text">
            <div class="modal-content">
              <h4 class="amber-text">Accès sécurisé</h4>
              <hr />
              <p class="dark-text text-lighteen-4 py-2 px-3 left-align">Je vous propose de renseigner <strong>les identifiants qui vous ont été transmis</strong>.</p>

              <form action="connexion.php" method="post">
                <div class="col s12 m6">
                  <div class="input-field col s12">
                    <input id="pseudo" type="text" name="pseudo" />
                    <label for="pseudo" class="amber-text">Pseudo</label>
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="input-field col s12">
                    <input id="password" type="password" name="mdp" />
                    <label for="password" class="amber-text">Password</label>
                  </div>
                </div>
                <div class="col s12 mt-3">
                  <button type="submit" class="waves-effect waves-light btn btn-block btn-small blue scale-transition btn-modal"><i class="material-icons left">lock</i>
                    Connectez-vous !!
                  </button>
                </div>
              </form>
            </div>
          </div><!-- Fin de la modal structure -->

        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <span>&copy; 2020-2021</span>
        <span class="center hide-on-small-only">HTML - CSS - JS - PHP - MySQL - Materialize</span>
        <a class="grey-text text-lighten-4 right" href="#!">By Alain Guillon ( Zyrass )</a>
      </div>
    </div>
  </footer>

  <!--JavaScript at end of body for optimized loading-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js" defer></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {

      // Constantes
      const elementSidenav = document.querySelectorAll('.sidenav');
      const elementTabs = document.querySelectorAll('.tab');
      const elementModal = document.querySelectorAll('.modal');

      M.AutoInit();

      let sidenav = M.Sidenav.init(elementSidenav, {});
      let tab = M.Tabs.init(elementTabs, {
        swipeable: true
      });
      let modal = M.Modal.init(elementModal, {
        opacity: 0.8,
      });

    });

    // Container Bouton
    const containerBTN = document.getElementById("btn_container");

    // Boutons
    const btnLogout = document.getElementById("logout");
    const btnAdd = document.getElementById("btnAdd");

    // Konami code
    const konami = "38,38,40,40,37,39,37,39,66,65,13";
    const troiswa = ["84,82,79,73,83,87,65,13", "99,87,65,13"];
    const eleanore = "69,76,50,65,78,79,82,69,13";

    let keysTaped = [];

    console.log(keysTaped.toString());

    window.addEventListener("keydown", $e => {

      console.log($e.keyCode);
      keysTaped.push($e.keyCode);
      console.log(keysTaped);

      if (keysTaped.toString().indexOf(konami) >= 0) {
        alert("Il est possible de saisir : '3wa' ou 'troiswa' pour obtenir une connexion au panel de modération");
        keysTaped = [];
      }

      if (keysTaped.toString().indexOf(eleanore) >= 0) {
        const btnAdmin = document.createElement("button");
        btnAdmin.setAttribute("class", "btn");
        btnAdmin.setAttribute("data-target", "modal3WA");
        btnAdmin.classList.add("btn-block", "btn-small", "waves-effect", "waves-light", "modal-trigger", "red", "red-darken-1", "white-text", "mt-1");
        btnAdmin.textContent = "Accès en tant qu'administrateur"

        containerBTN.appendChild(btnAdmin);

        setTimeout(() => {
          btnAdmin.remove();
        }, 4000);

        keysTaped = [];
      }

      if (keysTaped.toString().indexOf(troiswa[0]) >= 0 || keysTaped.toString().indexOf(troiswa[1]) >= 0) {

        const btn3WA = document.createElement("button");
        btn3WA.setAttribute("class", "btn");
        btn3WA.setAttribute("data-target", "modal3WA");
        btn3WA.classList.add("btn-block", "btn-small", "waves-effect", "waves-light", "modal-trigger", "amber", "brown-text", "text-darken-1", "mt-1");
        btn3WA.textContent = "Accès exclusif pour la 3WA"

        containerBTN.appendChild(btn3WA);

        setTimeout(() => {
          btn3WA.remove();
        }, 4000);

        keysTaped = [];
      }

      if (keysTaped.length > 11) {
        keysTaped = [];
      }

      if (keysTaped[0] === 13) {
        keysTaped = [];
      }

    }, true);

    // Deconnexion
    btnLogout.addEventListener("click", $e => {
      $e.preventDefault();
      document.location.href = "logout.php";
    });

    btnAdd.addEventListener("click", $e => {
      $e.preventDefault();
      document.location.href = "add.php";
    });
  </script>
  </body>

  </html>