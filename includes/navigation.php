<?php session_start(); ?>

  <!-- Navigation -->
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="index.php" id="brand" class="brand-logo">Z.Hymnes</a>
        <a href="#" data-target="mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="index.php"><i class="material-icons left">home</i> Accueil</a></li>
          <li>
            <a href="add.php" class="hide-on-large-only"><i class="material-icons left">add</i> Ajouter un hymne</a>
          </li>
          <?php if (isset($_SESSION['admin'])) : ?>
            <li><a href="panel-admin.php" class="white red-text"><i class="material-icons left">settings</i>Panel-Admin</a></li>
          <?php elseif (isset($_SESSION['modo'])) : ?>
            <li><a href="panel-admin.php" class="white blue-text"><i class="material-icons left">settings</i>Panel-Modération</a></li>
          <?php endif ?>
        </ul>
      </div>
    </nav>
  </div>

  <ul class="sidenav red lighten-2" id="mobile">
    <h3 class="center-align white-text">Menu</h3>
    <div class="divider"></div>
    <li><a href="index.php" class="center-align mt-2 white-text"><i class="material-icons left">home</i>Accueil</a>
    <li><a href="add.php" class="hide-on-large-only center-align white-text"><i class="material-icons left">add</i> Ajouter un hymne</a></li>
    <?php if (isset($_SESSION['admin'])) : ?>
      <li class="white"><a href="panel-admin.php" class="red-text center-align"><i class="material-icons left">settings</i>Panel-Admin</a></li>
    <?php elseif (isset($_SESSION['modo'])) : ?>
      <li class="white"><a href="panel-admin.php" class="blue-text center-align"><i class="material-icons left">settings</i>Panel-Modération</a></li>
    <?php endif ?>
  </ul>

  <!-- Breadcrumb -->
  <nav>
    <div class="nav-wrapper">
      <div class="col s12 ml-3 mt-2 mb-5 center-align">
        <a href="index.php" class="breadcrumb">Accueil</a>
        <a href="" class="breadcrumb blue-grey-text text-darken-4">

          <?php if ($page === "Nation") : ?>
            Hymne : <?= $resultat['country'] ?>
          <?php elseif ($page === "Ajouter") : ?>
            Ajouter un hymne
          <?php elseif ($page === "Modifier") : ?>
            Modifier un hymne
          <?php elseif ($page === "Supprimer") : ?>
            Supprimer un pays
          <?php elseif ($page === "Liste") : ?>
            Liste des pays
          <?php elseif ($page === "Panel-Admin") : ?>
            Panel pour Administrateur
          <?php elseif ($page === "Panel-Modération") : ?>
            Panel pour Modérateur
          <?php elseif ($page === "Panel-Visiteur") : ?>
            Panel pour Visiteur curieux
          <?php endif ?>

        </a>
      </div>
    </div>
  </nav>