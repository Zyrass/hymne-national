-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 18 sep. 2020 à 09:30
-- Version du serveur :  10.3.22-MariaDB-1ubuntu1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hymne`
--

-- --------------------------------------------------------

--
-- Structure de la table `nation`
--

CREATE TABLE `nation` (
  `id` int(11) NOT NULL,
  `flag` text NOT NULL COMMENT 'drapeau',
  `country` varchar(100) NOT NULL COMMENT 'pays',
  `title` varchar(100) NOT NULL COMMENT 'titre',
  `lyrics` text NOT NULL COMMENT 'paroles original',
  `translate` text NOT NULL COMMENT 'traduction française',
  `description` text NOT NULL COMMENT 'description',
  `wikipedia` varchar(255) NOT NULL COMMENT 'lien wikipedia',
  `video` varchar(255) NOT NULL COMMENT 'lien video'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `nation`
--
ALTER TABLE `nation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `nation`
--
ALTER TABLE `nation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
