<?php

session_start();

// connexion à la base de donnée
require_once './bdd/connexion.php';

// Requêtes pour sélectionner les tous les éléments pour afficher un maximum d'informations.
$select = "SELECT * FROM nation WHERE id >= 1 ORDER BY country LIMIT 0,20";
$req = $bdd->prepare($select);

// exécution de la requête + vérification qu'il n'y a pas de soucis
$executeIsOk = $req->execute();

if (!$executeIsOk) {
  $message = "Désolé une erreur a été rencontré lors de la requête";
}

// Récupération des données
$resultat = $req->fetchAll(PDO::FETCH_ASSOC);

// Combien de ligne sont-elles  récupéré ?
$combienDeLigne = count($resultat);

$page = "Liste";
include_once './includes/header.php';
include_once './includes/navigation.php';

?>

<div class="container my-5">
  <!-- Information -->
  <div class="row">
    <h2 class="blue white-text text-darkten-4 center-align p-1 my-5">Information</h2>
    <div class="col s12 m12 offset-l2 l8">
      <p class="flow-text amber-text text-lighten-1 px-1">
        <?php if ($combienDeLigne === 1) : ?>
          <strong>Il y a qu' <span class="red-text">une ( <?= $combienDeLigne ?> )</span> nation</strong> disponible d'enregistré, elle dispose de quelques informations particulière.
        <?php elseif ($combienDeLigne > 1) : ?>
          <strong>Il y a <span class="red-text"><?= $combienDeLigne ?></span> nations</strong> disponible, elles disposent toutes de quelques informations particulière.
        <?php else : ?>
          <strong>Actuellement il y a <span class="red-text">aucune</span> nation</strong> d'enregistré dans la base de donnée.
        <?php endif ?>
      </p>

      <?php if ($combienDeLigne >= 1) : ?>
        <blockquote class="blue-grey-text text-lighten-1">
          <p>
            Comme par exemple <strong>un lien vers le Wikipédia</strong> traitant sur la nation sélectionné, <strong>une vidéo</strong> avec <strong>les paroles incrusté</strong>. Egalement, il y a <strong>les paroles original et traduites en Français</strong>...<br /><br /> "...<em>du moins quand c'est possible</em>".
          </p>
        </blockquote>
      <?php endif ?>
    </div>
  </div><!-- /.row -->

  <section>
    <h2 class="blue-grey white-text text-darkten-4 center-align p-1 my-5">Liste - (
      <?php if ($combienDeLigne < 1) : ?>
        <span class="red-text "> <?= $combienDeLigne ?></span>
      <?php else : ?>
        <span class="amber-text "> <?= $combienDeLigne ?></span>
      <?php endif ?>
      )</h2>

    <div class="row">
      <?php foreach ($resultat as $key => $value) : ?>
        <!-- Nation -->
        <div class="col s12 m4 xl3">
          <div class="card z-depth-4 ">
            <div class="card-image">
              <img src="<?= $value['flag'] ?>" alt="Image drapeau du pays : <?= $value['country'] ?>">

              <?php if (isset($_SESSION['modo']) || isset($_SESSION['admin'])) : ?>
                <a class="btn-floating halfway-fab waves-effect waves-light brown accent-4 pulse" href="update.php?identifiant=<?= $value['id'] ?>&pays=<?= $value['country'] ?>"><i class="material-icons amber-text">mode_edit</i></a>
              <?php endif ?>

            </div>
            <div class="card-content">
              <h6>Titre de l'hymne national : <br /><span class="red-text"><?= $value['title'] ?></span></h6>
            </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn btn-small blue lighten-1 modal-trigger " href="nation.php?identifiant=<?= $value['id'] ?>" ;>
                <i class="material-icons left">link</i>Accedez au contenu
              </a>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div> <!-- /.row -->
  </section>
</div>

<?php include_once './includes/footer.php'; ?>