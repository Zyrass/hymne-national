<?php

session_start();

// Connexion à la base de donnée
include_once "./bdd/connexion.php";

// Requête pour sélectionner tous les Hymnes enregistré
$select_all = "SELECT * FROM nation WHERE id >= 1 ORDER BY country LIMIT 0,300";
$req = $bdd->prepare($select_all);

// Execute la requête
$executeIsOk = $req->execute();

if (!$executeIsOk) {
  $_SESSION['error'] = $error = "Désolé une erreur à été rencontré lors de l'exécution de la requête";
}

// Récupération des data
$resultat = $req->fetchAll(PDO::FETCH_OBJ);

// Combien de ligne disponible
$compteur = count($resultat);

if ($_SESSION['modo']) {
  $page = "Panel-Modération";
} elseif ($_SESSION['admin']) {
  $page = "Panel-Admin";
} else {
  $page = "Panel-Visiteur";
}

include_once './includes/header.php';
include_once './includes/navigation.php';

?>

<div class="container">
  <div class="row">

    <!-- Titre de la page pour admin -->
    <?php if ($_SESSION['admin']) : ?>
      <h1 class="red white-text center-align p-1">Panel Administrateur</h1>
    <?php elseif ($_SESSION['modo']) : ?>
      <h1 class="blue white-text center-align p-1">Panel Modérateur</h1>
    <?php else : ?>
      <h1 class="blue-grey white-text center-align p-1">Panel visiteur</h1>
    <?php endif ?>

    <!-- Message -->
    <div class="col s12 offset-m2 m8">
        <?php if ($_SESSION['admin']) : ?>
          <blockquote class="text-flow white-text">
            En tant qu'administrateur, vous allez pouvoir avoir accès à une action supplémentaire.
            La suppression d'un hymne national.
          </blockquote>
        <?php elseif ($_SESSION['modo']) : ?>
          <blockquote class="text-flow white-text">
            En tant que modérateur, vous allez pouvoir avoir accès à l'ajout ou bien la modification d'un hymne existant. Quant à la partie "suppression", celle-ci est réservé exclusivement à l'administrateur.
          </blockquote>
        <?php endif ?>
    </div>
  </div>

  <div class="row">
    <div class="col s12">
      <!-- Compteur -->
      <div class="m-1 panel-admin-info-number center-align">
        <span class="number amber-text"><?= $compteur ?></span><span class="panel-admin-info-text white-text"> hymnes enregistré dans la base de donnée</span>
      </div>
    </div>
  </div>

  <?php if ( $_SESSION['admin'] || $_SESSION['modo']) : ?>
  <div class="row">
    <div class="col s12 center-align">
      <!-- Ajouter -->
      <button class="btn btn-block btn-small waves-effect waves-light green green-lighteen-4" id="btnAdd"><i class="material-icons left">add</i>Ajouter un nouvel hymne</button>
    </div>
  </div>
  <?php endif ?>

  <div class="row">
    <div class="col s12 offset-m2 m8 mt-3">
      <table class="striped highlight centered">
        <caption class="my-2">Liste des <?= $compteur ?> hymnes national enregistré et réorganisé par nom du pays.</caption>
        <thead>
          <tr>
            <th>ID</th>
            <th>FLAG</th>
            <th>COUNTRY</th>
            <th>SHOW</th>
            <th>UPDATE</th>
            <th>DELETE</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($resultat as $key => $value) : ?>
            <tr>
              <td>#<?= $value->id ?></td>
              <td><img src="<?= $value->flag ?>" alt="Drapeau du pays : <?= $value->country ?>" width="64" /></td>
              <td><?= $value->country ?></td>
              <td>
                <a href="nation.php?identifiant=<?= $value->id ?>" class="btn btn-small waves-effect waves-light green">show</a>
              </td>
              <?php if ( $_SESSION['admin'] || $_SESSION['modo']) : ?>
              <td>
                <a href="update.php?identifiant=<?= $value->id ?>&pays=<?= $value->country ?>" class="btn btn-small waves-effect waves-light amber brown-text">update</a>
              </td>
              <?php else : ?>
              <td><i class="material-icons left">cancel</i>Pas d'accès</td>
              <?php endif ?>


              <?php if ( $_SESSION['admin'] ) : ?>
              <td>
                <a href="#modalSuppression" class="btn btn-small waves-effect waves-light red modal-trigger">delete</a>
              </td>
              <?php else : ?>
              <td><i class="material-icons left">cancel</i>Pas d'accès</td>
              <?php endif ?>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>

      <!-- Modal Structure -->
      <div id="modalSuppression" class="modal">
        <div class="modal-content">
          <h4>Confirmation de Suppression</h4>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates minus unde temporibus porro necessitatibus! Inventore repellendus sit aspernatur aliquam. Ex laudantium earum laborum quam vel.</p>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-close waves-effect waves-green btn-flat">Confirmez la suppression ici !!</a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include_once './includes/footer.php'; ?>
