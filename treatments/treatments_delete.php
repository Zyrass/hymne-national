<?php

  // Récupération des variables passé en GET
  $id = htmlspecialchars($_GET['identifiant']);
  $country = htmlspecialchars($_GET['pays']);

  // Connexion à la base de donnée
  require_once '../bdd/connexion.php';

  // Requête pour sélectionner l'id et le pays
  $select_sql_id_country = "SELECT id, country FROM nation WHERE id = :id AND country = :country LIMIT 1";
  $req = $bdd->prepare($select_sql_id_country);

  // Liaison des marqueurs
  $req->bindValue(':id', $id, PDO::PARAM_INT);
  $req->bindValue(':country', $country, PDO::PARAM_STR);

  // Exécution de la requête
  $executeIsOk = $req->execute();

  // Vérification de la bonne exécution de la sélection
  if (!$executeIsOk) {
    echo "<div class='debug'>";
      echo $message = "L'exécution n'a pas fonctionné.";
    echo "</div>";
    $req->closeCursor();
  }

  // Récupération du contenu de la base de donnée
  $resultat = $req->fetch(PDO::FETCH_OBJ);

  // Contrôle des données saisie avec les données récupérer en base de donnée
  if ($id === $resultat->id && isset($resultat->id) && isset($resultat->country) ) {

    $message = "Nickel on rentre dedans car le contrôle est bon";

    // A cette instant on peut delete la data sélectionné
    $delete_sql = "DELETE FROM nation WHERE id = :id LIMIT 1";
    $req_delete = $bdd->prepare( $delete_sql );

    // Liaison de l'id
    $req_delete->bindValue(":id", $id, PDO::PARAM_INT);

    // Exécution du code
    $executeIsOK_delete = $req_delete->execute();

    // Vérification de la bonne exécution de suppression
    if (!$executeIsOk) {
      $message = "L'exécution n'a pas fonctionné. Donc suppression non exécuté";
      $req->closeCursor();
    }

    // Une fois supprimer on est rediriger vers la page de suppression
    header("Location: ../delete.php");

  } else {

    // Le contrôle n'est pas bon on redirige vers la page de suppression.
    $message = "Le contrôle des valeurs n'est pas bon...";

    header("Location: ../delete.php");

  }


  // Fermeture de la connexion
  $req->closeCursor();

  $page = "Traitement suppression";
  include_once '../includes/header.php';
?>

  <div class="container">
    <div class="row">
      <div class="col s12">
        <h1 class="white-text">
          <span class="blue-grey-text text-lighten-1"><?= $resultat->id ?></span> - <?= $resultat->country ?>
        </h1>
        <p class="flow-text"><?= $message ?></p>
      </div>
    </div>
  </div>

<?php include_once "../includes/footer.php" ?>
