<?php

echo "<pre class='debug'>";
print_r($_POST);
echo "</pre>";

/*

Array
(
    [flag] => sdfsdfsdff
    [country] => sdfsdfsdf
    [title] => dfsdfsdfdf
    [wikipedia] => sdfsdfsdfsdf
    [youtube] => sdffdsfsd
    [description] => fsdfsdfsdff
    [lyrics] => sdfsdfsdf
    [translate] => sdfsdfsdf
)

*/

// traitement des données reçus
$flag = htmlspecialchars($_POST['flag']);
$country = htmlspecialchars($_POST['country']);
$title = htmlspecialchars($_POST['title']);
$wikipedia = htmlspecialchars($_POST['wikipedia']);
$youtube = htmlspecialchars($_POST['youtube']);
$description = htmlspecialchars($_POST['description']);
$lyrics = htmlspecialchars($_POST['lyrics']);
$translate = htmlspecialchars($_POST['translate']);

if (
  isset($flag) && !empty($flag) &&
  isset($country) && !empty($country) &&
  isset($title) && !empty($title) &&
  isset($wikipedia) && !empty($wikipedia) &&
  isset($youtube) && !empty($youtube) &&
  isset($description) && !empty($description) &&
  isset($lyrics) && !empty($lyrics) &&
  isset($translate) && !empty($translate)
) {

  echo $message = "Ok tout à été remplis";

  // Si tout les champs existent et qu'ils ne sont pas vide on peut se connecté à la base de donnée.
  include_once "../bdd/connexion.php";

  // Requête SQL d'insertion
  $insert_sql = "INSERT INTO nation (flag, country, title, wikipedia, video, description, lyrics, translate) VALUES (:flag, :country, :title, :wikipedia, :youtube, :description, :lyrics, :translate)";
  $request = $bdd->prepare($insert_sql);

  // Liaison des marqueurs
  $request->bindValue(':flag', $flag, PDO::PARAM_STR);
  $request->bindValue(':country', $country, PDO::PARAM_STR);
  $request->bindValue(':title', $title, PDO::PARAM_STR);
  $request->bindValue(':wikipedia', $wikipedia, PDO::PARAM_STR);
  $request->bindValue(':youtube', $youtube, PDO::PARAM_STR);
  $request->bindValue(':description', $description, PDO::PARAM_STR);
  $request->bindValue(':lyrics', $lyrics, PDO::PARAM_STR);
  $request->bindValue(':translate', $translate, PDO::PARAM_STR);

  // Exécution de la requête
  $executeIsOk = $request->execute();

  if (!$executeIsOk) {
    echo $message = "Désolé une erreur a été rencontré. (requête)";
  } else {
    echo $message = "Data envoyé avec succès";
  }

  // Si tout est OK on redirige vers la page d'accueil
  header("Location: ../index.php");
} else {
  echo $message = "Certain(s) champ(s) n'ont pas été remplis";
  header("Location: ../add.php");
}
