<?php

echo "<pre class='debug'>";
print_r($_POST);
echo "</pre>";

// traitement des données reçus en POST
$id = htmlspecialchars(trim($_POST['identifiant_secret']));
$flag = htmlspecialchars(trim($_POST['flag']));
$country = htmlspecialchars(trim($_POST['country']));
$title = htmlspecialchars(trim($_POST['title']));
$wikipedia = htmlspecialchars(trim($_POST['wikipedia']));
$video = htmlspecialchars(trim($_POST['youtube']));
$description = htmlspecialchars(trim($_POST['description']));
$lyrics = htmlspecialchars(trim($_POST['lyrics']));
$translate = htmlspecialchars(trim($_POST['translate']));

if (
  isset($id) && !empty($id) &&
  isset($flag) && !empty($flag) &&
  isset($country) && !empty($country) &&
  isset($title) && !empty($title) &&
  isset($wikipedia) && !empty($wikipedia) &&
  isset($video) && !empty($video) &&
  isset($description) && !empty($description) &&
  isset($lyrics) && !empty($lyrics) &&
  isset($translate) && !empty($translate)
) {

  echo $message = "Ok tout à été remplis";

  // Si tout les champs existent et qu'ils ne sont pas vide on peut se connecté à la base de donnée.
  include_once "../bdd/connexion.php";

  // Requête SQL d'insertion
  $update_sql = "UPDATE nation SET flag=:flag, country=:country, title=:title, wikipedia=:wikipedia, video=:youtube, description=:description, lyrics=:lyrics, translate=:translate WHERE id=:id LIMIT 1";
  $request = $bdd->prepare($update_sql);

  // Liaison des marqueurs
  $request->bindValue(':id', $id, PDO::PARAM_INT);
  $request->bindValue(':flag', $flag, PDO::PARAM_STR);
  $request->bindValue(':country', $country, PDO::PARAM_STR);
  $request->bindValue(':title', $title, PDO::PARAM_STR);
  $request->bindValue(':wikipedia', $wikipedia, PDO::PARAM_STR);
  $request->bindValue(':youtube', $video, PDO::PARAM_STR);
  $request->bindValue(':description', $description, PDO::PARAM_STR);
  $request->bindValue(':lyrics', $lyrics, PDO::PARAM_STR);
  $request->bindValue(':translate', $translate, PDO::PARAM_STR);

  // Exécution de la requête
  $executeIsOk = $request->execute();

  if (!$executeIsOk)
    echo $message = "Désolé une erreur a été rencontré. (requête)";
  else
    echo $message = "Data modifié avec succès";

  var_dump($request);

  // Si tout est OK on redirige vers la page d'accueil
  header("Location: ../index.php");
} else {
  echo $message = "Certain(s) champ(s) n'ont pas été remplis";
  header("Location: ../index.php");
}
