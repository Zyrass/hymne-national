<?php

session_start();

$id = htmlspecialchars($_GET['identifiant']);
$country = htmlspecialchars($_GET['pays']);

if (!isset($id)) {
  $_SESSION['errors'] = $error = "Vous n'avez pas les droits";
  header("Location: index.php");
}

if (isset($id) && !empty($id) && isset($country) && !empty($country)) {

  // Connexion à la base de donnée
  require_once "./bdd/connexion.php";

  // Requêtes SQL pour sélectionner toutes les données.
  $select_sql = 'SELECT * FROM nation WHERE id = :id AND country = :country LIMIT 1';
  $req = $bdd->prepare($select_sql);

  // Liaison du marqueur id
  $req->bindValue(':id', $id, PDO::PARAM_INT);
  $req->bindValue(':country', $country, PDO::PARAM_STR);

  // Exécution de la requête et vérification si tout est OK
  $executeIsOk = $req->execute();

  if (!$executeIsOk) {
    $message = "Désolé une erreur a été rencontré. (requête)";
  }

  // Récupération des données.
  $resultat = $req->fetch(PDO::FETCH_OBJ);

  // Contrôle de la bonne réception des données dans la variable résultat
  // echo '<pre class="debug">';
  // print_r($resultat);
  // echo '</pre>';

  // Vérification du nombre de ligne
  $nombreDeLignes = count($resultat);

  // Fermeture de la connexion à la base de donnée
  $req->closeCursor();
} else {

  header("Location: add.php");
}

$page = "Modifier";
include_once './includes/header.php';
include_once './includes/navigation.php';
?>


<div class="container">
  <div class="row">

  </div><!-- /.row -->

  <h1 class="white-text center-align">Modifier</h1>
  <hr class="my-5" />
  <blockquote class="blue-grey-text flow-text">
    <strong>Vous vous apprêtez à modifier ce pays : </strong> <span class="red-text text-lighten-1"><?= $resultat->country ?></span><br /><br />
    Pour que ces modifications soit efficacent, utilisez <strong>wikipédia</strong> et faîtes également des recherches sur <strong>google</strong>.
  </blockquote>

  <h2 class="white brown-text text-darken-4 center-align py-1 my-4">Formulaire</h2>

  <form action="treatments/treatments_update.php" method="post" class="col s12 mb-8">

    <!-- Idendifiant -->
    <input type="hidden" name="identifiant_secret" value="<?= $resultat->id ?>" />

    <!-- Drapeau -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">flag</i>
      <input placeholder="URL du drapeau" id="flag" type="url" class="validate" name="flag" value="<?= $resultat->flag ?>" />
      <label for="flag">Drapeau</label>
    </div>

    <!-- Pays -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">language</i>
      <input placeholder="Nom du pays" id="country" type="text" class="validate" name="country" value="<?= $resultat->country ?>" />
      <label for="country">Pays</label>
    </div>

    <!-- Titre de l'Hymne -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">title</i>
      <input placeholder="Titre d'origine" id="title" type="text" class="validate" name="title" value="<?= $resultat->title ?>" />
      <label for="title">Titre</label>
    </div>

    <!-- Lien wikipédia du pays concerné -->
    <div class="row">
      <div class="input-field col s12 mt-1">
        <i class="material-icons prefix amber-text">link</i>
        <input placeholder="Lien wikipédia du pays concerné" id="wikipedia" type="url" class="validate" name="wikipedia" value="<?= $resultat->wikipedia ?>" />
        <label for="wikipedia">wikipédia</label>
      </div>

      <div class="p-1">
        <strong>Exemple du lien wikipédia : </strong> <em class="cyan-text">https://fr.wikipedia.org/wiki/France</em>
      </div>
    </div>

    <!-- Lien vers la vidéo youtube -->
    <div class="row">
      <div class="input-field col s12 mt-1">
        <i class="material-icons prefix amber-text">link</i>
        <input placeholder="Lien vidéo youtube." id="youtube" type="url" class="validate" name="youtube" value="<?= $resultat->video ?>" />
        <label for="youtube">youtube</label>
      </div>
      <div class="p-1">
        <strong>Exemple du lien vidéo : </strong> <em class="cyan-text">https://www.youtube.com/embed/JtdAnUR4hVM</em>
      </div>
    </div>

    <!-- Description -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="description" class="materialize-textarea" name="description">
          <?= trim($resultat->description) ?>
        </textarea>
      <label for="description">Description</label>
    </div>

    <!-- Paroles original -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="lyrics" class="materialize-textarea" name="lyrics">
          <?= trim($resultat->lyrics) ?>
        </textarea>
      <label for="lyrics">Paroles original</label>
    </div>

    <!-- Paroles Traduite -->
    <div class="input-field col s12">
      <i class="material-icons prefix amber-text">description</i>
      <textarea id="translate" class="materialize-textarea" name="translate">
          <?= trim($resultat->translate) ?>
        </textarea>
      <label for="translate">Paroles tratuite en français</label>
    </div>

    <div class="row center-align mt-5">
      <div class="col s7">
        <button class="btn waves-effect waves-light amber ligthen-4"><i class="material-icons left hide-on-small-only">save</i>Enregistré<span class="hide-on-small-only"> les changements</span></button>
      </div>
      <div class="col s5">
        <button class="btn waves-effect waves-light red" type="reset"><i class="material-icons left hide-on-small-only">clear</i>Effacé</button>
      </div>
    </div>


  </form>
</div><!-- /.container -->

<?php include_once './includes/footer.php'; ?>