<?php

session_start();

// Sécurisation de l'idenditiant.
$id = htmlspecialchars($_GET['identifiant']);

// Test de l'existance de ma super gobal $_GET
if (isset($id) && !empty($id) && !is_null($id)) {

  global $id;

  // Connexion à la base de donnée.
  require_once './bdd/connexion.php';

  // Selection des id de la table "nation" afin de contrôler l'identifiant.
  $select_id_sql = 'SELECT * FROM nation WHERE id=:id';
  $req_id = $bdd->prepare($select_id_sql);

  // Liaison du marqueur
  $req_id->bindValue(':id', $id, PDO::PARAM_INT);

  // Exécution de la requête
  $executeIsOk = $req_id->execute();

  // Contrôle de l'exécution
  if (!$executeIsOk) {
    echo "<pre class='debug'>";
    echo $message = "La sélection de l'identifiant à rencontrer un problème.";
    echo "</pre>";
  }

  // Si l'exécution c'est déroulé sans problème on récupète la ligne.
  $resultat = $req_id->fetch(PDO::FETCH_ASSOC);

  // Si l'id en base de donnée n'existe pas alors on redirige vers la page d'accueil
  if (!$resultat['id']) {
    // echo "<pre class='debug'>";
    // echo $message = "Zut, l'identifiant n°$id n'existe pas en base de donnée...";
    // echo "</pre>";

    // Fermeture de la connexion
    $req_id->closeCursor();

    header("Location: index.php");
  } else {
    // echo "<pre class='debug'>";
    // echo $message = "Nickel ça marche, $id est bien un identifiant existant";
    // echo "</pre>";
  }

  // Fermeture de la connexion
  $req_id->closeCursor();
} else {

  // Une erreur à été soulevé.
  echo "<pre class='debug'>";
  var_dump("On ne rentre pas dans ma première condition...");
  echo "</pre>";

  $req_id->closeCursor();
}

$page = "Nation";
include_once './includes/header.php';
include_once './includes/navigation.php';
?>

<!-- Content -->
<div class="container">

  <!-- Drapeau | Pays -->
  <div class="row">
    <h1 class="white-text center-align">
      <img
        src="<?= $resultat['flag'] ?>"
        alt="Hymne : <?= $resultat['country'] ?>"
        width="64"
        height="48"
      />
      <?= $resultat['country'] ?>
    </h1>
    <hr />
  </div>

  <!-- Description | Vidéo -->
  <div class="row">

    <!-- description -->
    <div class="col s12 m6">
      <h4 class="amber brown-text lighteen-4 p-1 center-align"><i class="small material-icons left">description</i>Description</h4>
      <blockquote class="flow-text grey-text text-darkten-2 left-align">
        <?= $resultat['description'] ?>
      </blockquote>
    </div>

    <!-- Vidéo -->
    <div class="col s12 m6">
      <h4 class="amber brown-text ligtheen-4 p-1 center-align"><i class="small material-icons left">music_video</i>Vidéo</h4>
      <div class="video-container z-depth-4">
        <iframe width="853" height="480" src="<?= $resultat['video'] ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <!-- Titre de l'hymne -->
  <div class="row">
    <h3 class="white-text center-align mt-8">
      Titre de l'hymne national : <span class="amber-text"><?= $resultat['title'] ?></span>
    </h3>
  </div>

  <!-- Groupe Paroles Hymnes -->
  <div class="row">

    <!-- Titre -->
    <div class="col s12">
      <h4 class="amber brown-text center-align p-1"><i class="small material-icons left">import_contacts</i>Paroles</h4>
    </div>

    <!-- Paroles original -->
    <div class="col s12 m6 paroles-left">
      <div class="center-align mt-3 pb-3">
        <img src="<?= $resultat['flag'] ?>" alt="" width="128" class="z-depth-4">
      </div>
      <h5 class="amber-text center-align">
        Original
      </h5>
      <p class="flow-text lyrics py-5 right-align"><?= $resultat['lyrics'] ?></p>
    </div>

    <!-- Paroles Traduite -->
    <div class="col s12 m6 paroles-right">
      <div class="center-align mt-3 pb-3">
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/800px-Flag_of_France.svg.png" alt="drapeau de la france" width="128" class=" z-depth-4">
      </div>
      <h5 class="amber-text center-align">
        Traduction
      </h5>
      <?php if (empty($resultat['translate'])) : ?>
      <div class="red darken-4 amber-text py-2 px-1">
        <i class="small material-icons pr-1">info</i> Aucune traduction disponible
      </div>
      <?php else : ?>
        <p class="flow-text lyrics py-5"><?= $resultat['translate'] ?></p>
      <?php endif ?>
    </div>
  </div>

</div><!-- /.container -->

<?php include_once './includes/footer.php'; ?>