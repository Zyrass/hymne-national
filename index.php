<?php

session_start();

// Connexion à la base de donnée
require_once "./bdd/connexion.php";

// Requête rapide pour sélectionner uniquement tous les ID de la base de donnée
$idSelected = "SELECT id FROM nation WHERE id >= 1";
$ask = $bdd->prepare($idSelected);

// Requêtes SQL pour afficher uniquement les 4 dernières entrées dans la bdd.
$select = 'SELECT id, flag, country, title FROM nation WHERE id >= 1 LIMIT 0,4';
$req = $bdd->prepare($select);

// Exécution des requêtes et vérification si tout est OK
$executeIsOk1 = $ask->execute();
$executeIsOk2 = $req->execute();

if (!$executeIsOk1 || !$executeIsOk2) {
  $message = "Désolé une des deux requêtes à rencontré une erreur. (Voir requête)";
}

// Récupération des données.
$resultatID = $ask->fetchAll(PDO::FETCH_ASSOC);
$resultat = $req->fetchAll(PDO::FETCH_OBJ);

// Contrôle de la bonne réception des données dans la variable résultat
// echo '<pre class="debug">';
// print_r($resultatID);
// print_r($resultat);
// echo '</pre>';

// Vérification du nombre de ligne
$combienDeLignePourResultatID = count($resultatID);
$combienDeLignePourResultat = count($resultat);

// Fermeture des connexions à la base de donnée.
$ask->closeCursor();
$req->closeCursor();

$page = "Accueil";
include_once './includes/header.php';
?>

<?php if ($message) : ?>
  <div class="red amber-text text-lighten-4">
    <?= $message ?>
  </div>
<?php endif ?>

<div class="container">

  <!-- En-tête -->
  <header class="col s12 m4 center-align">
    <h1 class="white-text animate__animated animate__flipInY">Z.Hymnes</h1>
    <h5><?php
        if (isset($_SESSION['modo'])) {
          echo "<span class='blue-text text-lighteen-4'>vous êtes connecté en tant que modérateur</span>";
        } elseif (isset($_SESSION['admin'])) {
          echo "<span class='red-text text-lighteen-4'>vous êtes connecté en tant qu'administrateur</span>";
        }
        ?></h5>
    <div class="divider"></div>
    <blockquote class="flow-text grey-text text-darkten-2 left-align">
      Qui ne sait jamais un jour, posé la question sur ce que pouvait être exactement l'hymne national du pays rencontré !? Que ce soit niveau des paroles ou bien de la mélodie ou ce que sa pouvait bien dire.
    </blockquote>
  </header>

  <!-- Block Erreur Session -->
  <?php if ($_SESSION['errors']) : ?>
    <ul>
      <?php foreach ($_SESSION['errors'] as $key => $errorValue) : ?>
        <li><?= $errorValue ?></li>
      <?php endforeach ?>
    </ul>
  <?php endif ?>

  <!-- Moteur de recherche -->
  <h3 class="amber-text text-darkten-4 center-align p-1 my-3">Recherchez un hymne</h3>
  <div class="row">
    <form action="search.php" method="post">
      <div class="input-field col s12 offset-m2 m8">
        <i class="material-icons prefix amber-text text-lighteen-4">search</i>
        <input type="search" name="search" id="search" placeholder="Veuillez saisir le nom du pays que vous recherchez en français uniquement" />
        <label for="search" class="amber-text">Rechercher un pays spécifique :</label>
      </div>
    </form>
  </div>

  <!-- Les 4 derniers hymnes enregistré en base de donnée -->
  <h3 class="amber-text text-darkten-4 center-align p-1 my-3">Derniers Hymnes enregistré</h3>
  <div class="row">
    <?php foreach ($resultat as $key => $value) : ?>
      <div class="col s12 m6 xl3 center-align">
        <div class="card z-depth-4">
          <!-- Image représentative de la carte -->
          <div class="card-image animate__animated animate__backInLeft">
            <img src="<?= $value->flag ?>" alt="Image drapeau du pays : <?= $value->country ?>" />
          </div>
          <!-- Contenu de la carte -->
          <div class="card-content">
            <h6>Titre de l'hymne : <br /><span class="red-text"><?= $value->title ?></span></h6>
          </div>
        </div> <!-- /.card -->
      </div>
    <?php endforeach ?>
  </div>

  <!-- Voir la liste des hymnes actuellement enregistré sinon le bouton est désactiver partiellement -->
  <?php if ($combienDeLignePourResultatID > 0) : ?>
    <div class="row">
      <h3 class="amber-text text-darkten-4 center-align p-1 my-3">Liste des Hymnes</h3>
      <div class="col s12 center-align">
        <a class="waves-effect waves-light btn btn-large red darken-4 center-align animate__animated animate__pulse" href="list.php">
          <?php if ($combienDeLignePourResultatID === 1) : ?>
            <i class="material-icons left">link</i>Accedez à la liste avec <span class="amber-text"><?= $combienDeLignePourResultatID ?></span> seul hymne.
          <?php elseif ($combienDeLignePourResultatID > 1) : ?>
            <i class="material-icons left">link</i>Accedez aux <span class="amber-text"><?= $combienDeLignePourResultatID ?></span> hymnes
          <?php endif ?>
        </a>
      </div>
    </div>
  <?php endif ?>

</div>

<script>
  const search = document.getElementById("search");
  search.addEventListener("submit", $e => {
    $e.preventDefault;
  })
</script>

<?php include_once './includes/footer.php'; ?>