# Z.Hymnes (Zyrass Hymnes)

> Découvrez ici un site regroupant des informations sur tout les hymnes du monde entier.
> C'est un projet réalisé en PHP côté procédural. (Plus tard viendra la version Orienté Objet)

## Cheminement

1. Télécharger le projet
2. Inséré dans votre **phpmyadmin** le fichier sql fournit. (**nation.sql**)
3. Créer à la racine un répertoire **bdd**
4. Dans le répertoire **bdd** créé un fichier appelé **connexion.php**

### Le code du fichier "connexion.php"

```php
// Connexion sécurisé avec l'initialisation d'un tableau d'erreur via PDO
define('HOST', 'mysql:host=localhost;dbname=hymne');
define('USER', 'VOTRE PSEUDO UTILISE POUR VOUS CONNECTER');
define('PASSWORD', 'VOTRE MOT DE PASSE');

try {
  $bdd = new PDO(HOST, USER, PASSWORD);
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	die('Erreur de connexion : ' . $e->getMessage());
}
```

## Comment ajouter/supprimer un contenu

Ici il est question d'un **gros délire que je me suis fait**. En sommes, j'ai utilisé le principe d'un konami code pour afficher ou exécuter des actions.
Trois séquences existent afin de faire tel ou tel exécution.

> Chaque séquence doit se taper dans un espace vide, sans la virgule.

1. Avec vôtre clavier : `Haut, Haut, bas, bas, gauche, droite, gauche, droite, B, A, touche entrée` donne accès à une alerte avec les différentes options possible.
2. Avec vôtre clavier : `3, W, A, touche entrée` affiche dans le pied de page un bouton pour la connexion spécifique pour la 3WA.
3. Avec vôtre clavier : `J, E, S, U, I, S, D, I, S, P, O, touche entrée` permet d'afficher un bouton pour la connexion au panel d'administration.

## Et la modification dans tout ça

Là, rien de plus simple, il vous faut simplement cliqué sur le petit bouton qui sera disponible sur chaque carte représantant le pays.
Bien entendu, cette option ne sera visible que si vous êtes connecté en tant que modérateur.

## La base de donnée au départ est vide

> C'est volontaire pour que vous puissiez voir les changements apporté sur plusieurs pages en fonction du nombre de pays enregistré.
> 0, vous avez un message
> 1, vous avez un autre message
> plus de 1, vous avez encore un autre message.

## Pourquoi ce projet

Ce projet est avant tout un projet pour décrocher un diplôme. (__Je rajouterai la partie POO quand j'aurais un peu plus de temps pour moi__);

## Combien de temps pour réaliser ce projet

> Entre les déplacements à l'école, au collège, au travail de ma conjointe, la salle de sport, les courses imprévue...
> Je n'avais pas beaucoup de temps pour codé. Tout de même je me suis fixé un délai de 6 jours pour réalisé celui-ci.
> La partie "back-end" a été fini en 6 jours sans pousser les vérifications.
> La partie mobile était fini parallèlement au back avec le framework materialize

## Que reste-il à faire

- ~~**La partie CSS pour les tablettes et +** pour tenir le projet sur 6 jours.~~
- Ajouter un message flash quand une erreur à été rencontré.
- Sécuriser le mot de passe en base de donnée. (Dans le panel admin uniquement)

## Comptes-tu ajouter des options supplémentaire

La réponse est oui !

- J'ai pour idée d'ajouter par la suite sur la page d'accueil un filtre pour filtrer les pays par continent
  - Ce qui impliquera de modifier : base de donnée et code CSS
  - ~~Un moteur de recherche pour retrouver un pays directement.~~
  - ~~Ajout d'une connexion avec le choix soit modérateur, soit administrateur~~.
