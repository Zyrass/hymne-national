<?php

$search = htmlspecialchars(trim(ucfirst($_POST['search'])));

if (isset($search) && !empty($search)) {

  $success = "Traitement OK on continue";

  // connexion à la base de donnée
  include_once './bdd/connexion.php';

  // Requête pour retrouver le nom du pays
  $select_country = "SELECT id, country FROM nation WHERE country LIKE :country LIMIT 0,1 ";
  $req = $bdd->prepare($select_country);

  // Liaison du marqueur
  $req->bindValue(":country", $search, PDO::PARAM_STR);

  // Exécution de la requête
  $executeIsOk = $req->execute();

  // vérrification si la requête est correctement passé
  if (!$executeIsOk) {
    echo $error = "Désolé la requête à rencontré un problème";
  }

  // récupération du pays
  $resultat = $req->fetch(PDO::FETCH_OBJ);

  // Combien de ligne récupérer
  $combienDeLigne = count($resultat);

  if ($combienDeLigne === 1) {
    if ($search === $resultat->country) {
      header("Location: nation.php?identifiant=$resultat->id");
    } else {
      echo $error = "Le pays n'existe pas";
      header("Location: index.php");
    }
  } else {
    echo $error = "Désolé le résultat n'est pas bon.";
  }
} else {
  $error = "Désolé le champ est requis";
  header("Location: index.php");
}
